//
//  ViewController.m
//  Assign4
//
//  Created by Trevor Eckhardt on 6/4/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    ViewController1* vc1 = [ViewController1 new];
    ViewController2* vc2 = [ViewController2 new];
    
    //self.view.backgroundColor = [UIColor primaryBlueColor];
    
    //TabBar
    NSArray* tabVControllers = @[vc1, vc2];
    
    [self setViewControllers:tabVControllers];
    
    vc1.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Table" image:nil tag:0];
    vc2.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Recipe" image:nil tag:1];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
