//
//  UIColor+CustomColors.h
//  Assign4
//
//  Created by Trevor Eckhardt on 6/4/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+(UIColor*) primaryBlueColor;
+(UIColor*) primaryRedColor;

@end
