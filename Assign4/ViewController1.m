//
//  ViewController1.m
//  Assign4
//
//  Created by Trevor Eckhardt on 6/4/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController1.h"
#import "ViewController2.h"

@interface ViewController1 (){
    NSArray* recipes;
}

@end

@implementation ViewController1

- (void)viewDidLoad {
    [super viewDidLoad];
    //Loads data from plist, creates objects, loads TableView
    
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor primaryBlueColor];
    
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    NSArray* temp = [[NSArray alloc] initWithContentsOfFile:filePath];
    NSMutableArray* rec = [NSMutableArray new];
    
    for (NSDictionary* a in temp)
    {
        CustomObject* cust = [CustomObject new];
        cust.name = [a objectForKey:@"Name"];
        cust.desc = [a objectForKey:@"Description"];
        cust.image = [a objectForKey:@"Image"];
        
        [rec addObject:cust];
    }
    
    recipes = [[NSArray alloc] initWithArray:rec copyItems:NO];
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 20) style: UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
}

//Notifications
-(void)btnTouched:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"recipeChanged" object:@"Button Touched!"];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return recipes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    //NSDictionary* infoDictionary = recipes[indexPath.row][@"name"];
    
    cell.textLabel.text = [[recipes objectAtIndex:indexPath.row] name];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //NSDictionary* infoDictionary = recipes[indexPath.row];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"recipeChanged" object:[recipes objectAtIndex:indexPath.row]];
    
    ViewController2* info = [ViewController2 new];
    
    //info.informationDictionary = infoDictionary;
    [self.navigationController pushViewController:info animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end