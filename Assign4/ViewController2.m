//
//  ViewController2.m
//  Assign4
//
//  Created by Trevor Eckhardt on 6/4/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController2.h"

@implementation ViewController2

NSArray* recipes;

-(void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //Listens to message, replaces data
    
    self.view.backgroundColor = [UIColor primaryRedColor];
    
    //recipes = [NSMutableArray arrayWithContentsOfFile:@"Property List.plist"];
    
    //self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationRecieved:) name:@"recipeChanged" object:nil];
    
    CGFloat height = self.view.frame.size.height - 65;
    
    UILabel* nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 30)];
    nameLbl.text = [[recipes objectAtIndex:0] objectForKey:@"name"];
    nameLbl.textAlignment = NSTextAlignmentCenter;
    nameLbl.textColor = [UIColor redColor];
    [self.view addSubview:nameLbl];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 65 + (height * 5/48), self.view.frame.size.width, (height * 18/48))];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [iv setImage:[UIImage imageNamed: [[recipes objectAtIndex:0] objectForKey:@"image"]]];
    [self.view addSubview:iv];
    
    UITextView* labelDesc = [[UITextView alloc]initWithFrame:CGRectMake(0, 65 + (height * 24/48), self.view.frame.size.width, (height * 23/48))];
    labelDesc.text = [[recipes objectAtIndex:0] objectForKey:@"description"];
    labelDesc.textAlignment = NSTextAlignmentLeft;
    labelDesc.textColor = [UIColor blackColor];
    labelDesc.editable = NO;
    [self.view addSubview:labelDesc];
}

-(void)notificationRecieved:(NSNotification*)n
{
//    CGFloat height = self.view.frame.size.height - 65;
//    
//    UILabel* nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 30)];
//    nameLbl.text = [n.object objectForKey:@"name"];
//    nameLbl.textAlignment = NSTextAlignmentCenter;
//    nameLbl.textColor = [UIColor redColor];
//    [self.view addSubview:nameLbl];
//    
//    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 65 + (height * 5/48), self.view.frame.size.width, (height * 18/48))];
//    iv.contentMode = UIViewContentModeScaleAspectFit;
//    [iv setImage:[UIImage imageNamed: [n.object objectForKey:@"image"]]];
//    [self.view addSubview:iv];
//    
//    UITextView* labelDesc = [[UITextView alloc]initWithFrame:CGRectMake(0, 65 + (height * 24/48), self.view.frame.size.width, (height * 23/48))];
//    labelDesc.text = [n.object objectForKey:@"description"];
//    labelDesc.textAlignment = NSTextAlignmentLeft;
//    labelDesc.textColor = [UIColor blackColor];
//    labelDesc.editable = NO;
//    [self.view addSubview:labelDesc];
    
    //NSLog(@"%@", n.object);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end