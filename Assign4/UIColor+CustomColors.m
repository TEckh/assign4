//
//  UIColor+CustomColors.m
//  Assign4
//
//  Created by Trevor Eckhardt on 6/4/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+(UIColor*) primaryBlueColor
{
    return [UIColor colorWithRed:0 green:1 blue:1 alpha:1];
}

+(UIColor*) primaryRedColor
{
    return [UIColor colorWithRed:1 green:0 blue:0 alpha:1];
}

@end
