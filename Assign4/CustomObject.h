//
//  CustomObject.h
//  Assign4
//
//  Created by Trevor Eckhardt on 6/4/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomObject : NSObject

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* desc;
@property (nonatomic, strong) NSString* image;

@end
