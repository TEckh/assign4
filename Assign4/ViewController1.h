//
//  ViewController1.h
//  Assign4
//
//  Created by Trevor Eckhardt on 6/4/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#ifndef Assign4_ViewController1_h
#define Assign4_ViewController1_h


#endif
#import <UIKit/UIKit.h>

@interface ViewController1 : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end